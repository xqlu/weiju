package com.huixi.microspur.sysadmin.service;

import cn.binarywang.wx.miniapp.bean.WxMaJscode2SessionResult;
import cn.binarywang.wx.miniapp.bean.WxMaUserInfo;
import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.microspur.commons.page.PageData;
import com.huixi.microspur.commons.page.PageQuery;
import com.huixi.microspur.sysadmin.pojo.entity.user.WjUser;
import com.huixi.microspur.sysadmin.pojo.vo.user.UserDataVO;
import com.huixi.microspur.sysadmin.pojo.vo.user.UserPageVo;

/**
 * <p>
 * 用户信息表 服务类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjUserService extends IService<WjUser> {


    /**
     * 根据code 获取session_key、open_id
     *
     * @param code 用户code
     * @return java.lang.String
     * @Author 李辉
     * @Date 2019/11/23 4:31
     **/
    WxMaJscode2SessionResult getWxSession(String code);


    /**
     * 获取用户加密信息 后的信息
     *
     * @param encryptedData
     * @param session_key
     * @param ivKey
     * @return WxDecodeUserInfoDTO
     * @Author 叶秋
     * @Date 2020/2/5 13:44
     **/
    WxMaUserInfo getEncryptionUserInfo(String encryptedData, String session_key, String ivKey);


    /**
     * 根据用户code 获取用户的 openId 和 sessionKey , 来确定有没有授权过。 如果授权发放用户信息 ，没有就创建一个用户
     *
     * @param code 用户小程序的code
     * @return WjUser 用户信息类
     * @Author 叶秋
     * @Date 2020/2/1 20:17
     **/
    WjUser detectionUserAuthorization(String code);


    /**
     * 根据用户id 查询用户信息 包括需要查询其他表的一些数据（点赞、动态...）
     *
     * @param userId
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     * @Author 叶秋
     * @Date 2020/6/1 21:55
     **/
    UserDataVO getByIdUserData(String userId);

    /**
     *  封禁用户
     *
     * @param userId 用户id
     */
    void banUser(String userId);

    /**
     *  分页查询用户
     * @param pageQuery 分页条件
     */
    PageData<UserPageVo> page(PageQuery pageQuery);
}

package com.huixi.microspur.sysadmin.pojo.vo.user;

import com.huixi.microspur.sysadmin.pojo.entity.user.WjUser;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

@Builder
@Data
public class UserPageVo implements Serializable {

    @ApiModelProperty(value = "id")
    private String userId;

    @ApiModelProperty(value = "昵称")
    private String nickName;

    @ApiModelProperty(value = "真实姓名")
    private String realName;

    @ApiModelProperty(value = "头像对应的URL地址")
    private String headPortrait;

    @ApiModelProperty(value = "性别 改用String，活泼一点。自定义都可以")
    private String sex;

    @ApiModelProperty(value = "手机号")
    private String mobile;

    @ApiModelProperty(value = "公司")
    private String company;

    @ApiModelProperty(value = "电子邮箱")
    private String email;

    @ApiModelProperty(value = "个性签名（冗余）")
    private String signature;

    @ApiModelProperty(value = "简介")
    private String introduce;

    @ApiModelProperty(value = "地址")
    private String address;

    @ApiModelProperty(value = "身份证号")
    private String identityCard;

    @ApiModelProperty(value = "创建时间")
    public LocalDateTime createTime;

    @ApiModelProperty(value = "修改时间")
    private LocalDateTime updateTime;

    @ApiModelProperty(value = "是否启用 Y 启用， N 禁用")
    private String enableFlag;

    @ApiModelProperty(value = "自己发送诉求的次数")
    private Integer appealCount;

    @ApiModelProperty(value = "自己发送动态的数量")
    private Integer dynamicCount;

    @ApiModelProperty(value = "被对方点赞的次数")
    private Integer endorseCount;
}

package com.huixi.microspur.sysadmin.pojo.vo.dynamic;

import com.huixi.microspur.sysadmin.pojo.entity.dynamic.WjDynamic;
import lombok.Builder;
import lombok.Data;

/**
 * @author wuyiliang
 * @date 2020/8/25 11:57
 */
@Builder
@Data
public class DynamicInfoVO {

    private String content;

    private String url;

    private String fileType;

    private Integer endorseCount;

    private Integer commentCount;

    private Integer browseCount;


    public static DynamicInfoVO convert(WjDynamic dynamic) {
        if (dynamic == null) {
            return null;
        }
        return DynamicInfoVO.builder()
                .content(dynamic.getContent())
                .url(dynamic.getUrl())
                .fileType(dynamic.getFileType())
                .browseCount(dynamic.getBrowseCount())
                .endorseCount(dynamic.getEndorseCount())
                .commentCount(dynamic.getCommentCount()).build();
    }
}

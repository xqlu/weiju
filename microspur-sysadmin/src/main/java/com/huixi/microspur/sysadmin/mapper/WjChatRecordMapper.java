package com.huixi.microspur.sysadmin.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.huixi.microspur.sysadmin.pojo.entity.chat.WjChatRecord;

/**
 * <p>
 * 聊天室-聊天记录 Mapper 接口
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjChatRecordMapper extends BaseMapper<WjChatRecord> {

}

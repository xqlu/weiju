package com.huixi.microspur.sysadmin.pojo.entity.appeal;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.huixi.microspur.commons.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 *  用来判断是否已经点赞
 * @Author 叶秋
 * @Date 2020/3/15 15:14
 * @param
 * @return
 **/
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("wj_appeal_endorse")
@ApiModel(value="用来判断是否已经点赞", description="诉求点赞记录表")
public class WjAppealEndorse extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "诉求点赞记录表的id")
    @TableId(value = "appeal_endorse_id", type = IdType.ASSIGN_UUID)
    private String appealEndorseId;

    @ApiModelProperty(value = "诉求id")
    @TableField("appeal_id")
    private String appealId;

    @ApiModelProperty(value = "点赞的用户id")
    @TableField("user_id")
    private String userId;

    @ApiModelProperty(value = "创建人(可以是名称 或者 uuid)")
    @TableField("create_by")
    private String createBy;

    @ApiModelProperty(value = "修改人(可以是名称 或者 uuid)")
    @TableField("update_by")
    private String updateBy;


}

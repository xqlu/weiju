package com.huixi.microspur.sysadmin.pojo.entity.saying;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.huixi.microspur.commons.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 语录表
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("wj_saying")
@ApiModel(value="WjSaying对象", description="语录表")
public class WjSaying extends BaseEntity {

    private static final long serialVersionUID=1L;

    @ApiModelProperty(value = "语录id")
    @TableId("say_id")
    private String sayId;

    @ApiModelProperty(value = "用户id")
    @TableField("user_id")
    private String userId;

    @ApiModelProperty(value = "标题")
    @TableField("title")
    private String title;

    @ApiModelProperty(value = "封面（URL地址）")
    @TableField("cover")
    private String cover;

    @ApiModelProperty(value = "内容")
    @TableField("content")
    private String content;

    @ApiModelProperty(value = "点赞数")
    @TableField("support_num")
    private Integer supportNum;

    @ApiModelProperty(value = "评论数")
    @TableField("comment_num")
    private Integer commentNum;

    @ApiModelProperty(value = "分享数")
    @TableField("share_num")
    private Integer shareNum;

    @ApiModelProperty(value = "创建人")
    @TableField("create_by")
    private String createBy;

    @ApiModelProperty(value = "修改人")
    @TableField("update_by")
    private String updateBy;



}

package com.huixi.microspur.sysadmin.pojo.entity.sys;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.huixi.microspur.commons.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 叶秋
 * @since 2020-07-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_role")
@ApiModel(value="SysRole对象", description="")
public class SysRole extends BaseEntity {

    private static final long serialVersionUID=1L;

    @TableId("id")
    private String id;

    @TableField("role_name")
    private String roleName;

    @TableField("remark")
    private String remark;

    @TableField("create_by")
    private String createBy;

    @TableField("update_by")
    private String updateBy;



}

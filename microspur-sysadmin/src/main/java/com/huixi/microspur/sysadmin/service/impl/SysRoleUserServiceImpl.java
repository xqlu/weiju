package com.huixi.microspur.sysadmin.service.impl;

import com.huixi.microspur.sysadmin.pojo.entity.sys.SysRoleUser;
import com.huixi.microspur.sysadmin.mapper.SysRoleUserMapper;
import com.huixi.microspur.sysadmin.service.SysRoleUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 叶秋
 * @since 2020-07-24
 */
@Service
public class SysRoleUserServiceImpl extends ServiceImpl<SysRoleUserMapper, SysRoleUser> implements SysRoleUserService {

}

package com.huixi.microspur.sysadmin.pojo.entity.sys;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.huixi.microspur.commons.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author 叶秋
 * @since 2020-07-24
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("sys_menu")
@ApiModel(value="SysMenu对象", description="")
public class SysMenu extends BaseEntity {

    private static final long serialVersionUID=1L;

    @TableId("id")
    private String id;

    @TableField("name")
    private String name;

    @TableField("p_id")
    private String pId;

    @TableField("url")
    private String url;

    @ApiModelProperty(value = "排序字段")
    @TableField("order_num")
    private Integer orderNum;

    @ApiModelProperty(value = "图标")
    @TableField("icon")
    private String icon;

    @TableField("create_by")
    private String createBy;


    @TableField("update_by")
    private String updateBy;


    @ApiModelProperty(value = "权限")
    @TableField("permission")
    private String permission;

    @ApiModelProperty(value = "1栏目2菜单")
    @TableField("menu_type")
    private Integer menuType;




}

package com.huixi.microspur.sysadmin.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.huixi.microspur.sysadmin.pojo.entity.appeal.WjAppealComment;

/**
 * <p>
 * 诉求-评论 服务类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
public interface WjAppealCommentService extends IService<WjAppealComment> {

}

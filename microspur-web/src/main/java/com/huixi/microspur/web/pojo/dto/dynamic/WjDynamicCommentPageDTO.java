package com.huixi.microspur.web.pojo.dto.dynamic;

import com.huixi.microspur.commons.page.PageQuery;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 *  动态评论 分页VO类
 * @Author 叶秋
 * @Date 2020年8月8日21:15:11
 * @param
 * @return
 **/
@Data
@ApiModel(value = "动态分页查询所需的", description = "动态分页查询所需的, 分页不传有默认值")
public class WjDynamicCommentPageDTO {

    @NotNull(message = "动态id为空")
    @ApiModelProperty(value = "动态id", required = true)
    private String dynamicId;


    private PageQuery pageQuery;

}

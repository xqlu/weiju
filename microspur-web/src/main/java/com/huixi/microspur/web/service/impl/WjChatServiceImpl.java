package com.huixi.microspur.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.microspur.web.pojo.entity.appeal.WjAppeal;
import com.huixi.microspur.web.pojo.entity.chat.WjChat;
import com.huixi.microspur.web.pojo.entity.chat.WjChatUser;
import com.huixi.microspur.web.mapper.WjChatMapper;
import com.huixi.microspur.web.service.WjAppealService;
import com.huixi.microspur.web.service.WjChatService;
import com.huixi.microspur.web.service.WjChatUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 聊天室（聊天列表） 服务实现类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Service
public class WjChatServiceImpl extends ServiceImpl<WjChatMapper, WjChat> implements WjChatService {

    @Resource
    private WjChatUserService wjChatUserService;

    @Resource
    private WjAppealService wjAppealService;

    @Resource
    private WjChatService wjChatService;


    /**
     * 根据用户id 查询用户所有的聊天列表
     *
     * @param userId
     * @return java.util.List<com.huixi.microspur.web.pojo.entity.chat.WjChat>
     * @Author 叶秋
     * @Date 2020/4/23 23:19
     **/
    @Override
    public List<WjChat> queryAllChat(String userId) {

        List<WjChat> wjChatList = new ArrayList<>();

        QueryWrapper<WjChatUser> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("user_id", userId);

        List<WjChatUser> list = wjChatUserService.list(objectQueryWrapper);

        if (list!=null) {

            for (WjChatUser wjChatUser : list) {

                WjChat byId = getById(wjChatUser.getChatId());

                wjChatList.add(byId);


            }

            return wjChatList;

        }

        return wjChatList;


    }



    /**
     *  创建诉求聊天室
     * @Author 叶秋
     * @Date 2020/7/9 22:49
     * @param appealId 诉求id
     * @param userId 调用这个方法的用户
     * @return java.lang.Boolean
     **/
    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean createAppealChat(String appealId, String userId){

        // 查询发送改诉求的用户id
        WjAppeal byId = wjAppealService.getById(appealId);
        String userId1 = byId.getUserId();


        // 创建聊天室 / 聊天室的头像暂时定死
        try {
            WjChat wjChat = new WjChat().setAppealId(appealId)
                    .setType("one").setTitle(byId.getTitle()).setPeopleNumber(1)
                    .setUrl("https://weiju-wechat.oss-cn-shenzhen.aliyuncs.com/logo.png");

            boolean save = wjChatService.save(wjChat);

            if(save){

                // 创建对应的关系
                WjChatUser wjChatUser = new WjChatUser()
                        .setChatId(wjChat.getChatId())
                        .setUserId(userId);
                WjChatUser wjChatUser1 = new WjChatUser()
                        .setChatId(wjChat.getChatId())
                        .setUserId(userId1);


                boolean save1 = wjChatUserService.save(wjChatUser);
                boolean save2 = wjChatUserService.save(wjChatUser1);

                if(save1 && save2){
                    return true;
                }else{
                    throw new Exception("保存失败哦");
                }

            }
        } catch (Exception e) {
            throw new RuntimeException();
        }


        return false;
    }


}

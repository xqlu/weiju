package com.huixi.microspur.web.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.microspur.web.pojo.entity.user.WjUserTrack;
import com.huixi.microspur.web.mapper.WjUserTrackMapper;
import com.huixi.microspur.web.service.WjUserTrackService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户足迹表 服务实现类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Service
public class WjUserTrackServiceImpl extends ServiceImpl<WjUserTrackMapper, WjUserTrack> implements WjUserTrackService {

}

package com.huixi.microspur.web.controller.dynamic;


import cn.hutool.core.bean.BeanUtil;
import com.huixi.microspur.commons.base.BaseController;
import com.huixi.microspur.commons.errorcode.ErrorCodeEnum;
import com.huixi.microspur.commons.page.PageData;
import com.huixi.microspur.commons.util.wrapper.Wrapper;
import com.huixi.microspur.web.pojo.dto.dynamic.AddDynamicDTO;
import com.huixi.microspur.web.pojo.dto.dynamic.WjDynamicPageDTO;
import com.huixi.microspur.web.pojo.entity.dynamic.WjDynamic;
import com.huixi.microspur.web.service.WjDynamicService;
import com.huixi.microspur.web.util.CommonUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 动态表 前端控制器
 * </p>
 *
 * @author 叶秋
 * @since 2020-01-17
 */
@Slf4j
@RestController
@RequestMapping("/wjDynamic")
@Api(tags = "动态模块")
public class WjDynamicController extends BaseController {


    @Resource
    private WjDynamicService wjDynamicService;



    /**
     *  添加动态, (先只开放一张图片、暂不开放标签)
     * @Author 叶秋
     * @Date 2020/7/22 14:41
     * @param addDynamicDTO
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    @PostMapping("/addDynamic")
    @ApiOperation(value = "添加动态, (先只开放一张图片、暂不开放标签)")
    public Wrapper addDynamic(@RequestBody AddDynamicDTO addDynamicDTO){

        addDynamicDTO.setUserId(CommonUtil.getNowUserId());

        WjDynamic wjDynamic = new WjDynamic();
        BeanUtil.copyProperties(addDynamicDTO, wjDynamic);

        boolean save = wjDynamicService.save(wjDynamic);

        return Wrapper.ok(save);
    }



    /**
     * 动态不进行修改操作，因为就是一个短暂的交流展示，直接删除即可。
     * @Author 叶秋
     * @Date 2020/7/22 14:43
     * @param
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    @DeleteMapping("/dynamic/{dynamicId}")
    @ApiOperation(value = "删除动态")
    public Wrapper deleteDynamic(@PathVariable String dynamicId){

        String nowUserId = CommonUtil.getNowUserId();
        Boolean aBoolean = wjDynamicService.judgeIsMeDynamic(nowUserId, dynamicId);
        if(!aBoolean){
            return Wrapper.error(ErrorCodeEnum.DA_WEI_TIAN_LONG);
        }

        boolean b = wjDynamicService.removeById(dynamicId);

        return Wrapper.ok(b);
    }


    /**
     *  分页查询动态
     * @Author 叶秋
     * @Date 2020/4/19 21:12
     * @param
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    @PostMapping("/pageDynamic")
    @ApiOperation(value = "分页查询动态")
    public Wrapper queryPageWjDynamic(@RequestBody WjDynamicPageDTO wjDynamicPageVO){

        String nowUserId = CommonUtil.getNowUserId();
        wjDynamicPageVO.setUserId(nowUserId);

        PageData pageData = wjDynamicService.listPageDynamic(wjDynamicPageVO);


        return Wrapper.ok(pageData);
    }



    
    /**
     *  分页查询自己的诉求
     * @Author 叶秋 
     * @Date 2020/7/16 21:05
     * @param wjDynamicPageVO
     * @return com.huixi.microspur.commons.util.wrapper.Wrapper
     **/
    @PostMapping("/myDynamic")
    @ApiOperation(value = "分页查询自己的动态")
    public Wrapper queryPageMyDynamic(@RequestBody WjDynamicPageDTO wjDynamicPageVO){

        String nowUserId = CommonUtil.getNowUserId();
        wjDynamicPageVO.setUserId(nowUserId);

        PageData pageData = wjDynamicService.listPageDynamic(wjDynamicPageVO);


        return Wrapper.ok(pageData);
    }




}


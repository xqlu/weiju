package com.huixi.microspur.web.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.huixi.microspur.web.pojo.entity.appeal.WjAppealMaterial;
import com.huixi.microspur.web.mapper.WjAppealMaterialMapper;
import com.huixi.microspur.web.service.WjAppealMaterialService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 诉求素材表-存储素材涉及的图片，或者大文件 服务实现类
 * </p>
 *
 * @author xzl
 * @since 2020-01-17
 */
@Service
public class WjAppealMaterialServiceImpl extends ServiceImpl<WjAppealMaterialMapper, WjAppealMaterial> implements WjAppealMaterialService {

    @Override
    public List<WjAppealMaterial> listByAppealId(String appealId) {

        QueryWrapper<WjAppealMaterial> objectQueryWrapper = new QueryWrapper<>();
        objectQueryWrapper.eq("appeal_id",appealId);

        List<WjAppealMaterial> list = list(objectQueryWrapper);

        return list;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public Boolean replaceBeforeMaterial(String appealId, String[] materialList) {

        // 删除之前的
        QueryWrapper<WjAppealMaterial> wjAppealMaterialQueryWrapper = new QueryWrapper<>();
        wjAppealMaterialQueryWrapper.eq("appeal_id", appealId);

        boolean remove = remove(wjAppealMaterialQueryWrapper);

        boolean save = false;
        // 赋值现在的
        for (String s : materialList) {

            WjAppealMaterial wjAppealMaterial = new WjAppealMaterial()
                    .setAppealId(appealId).setUrl(s);
            save = save(wjAppealMaterial);

        }




        return save;
    }
}

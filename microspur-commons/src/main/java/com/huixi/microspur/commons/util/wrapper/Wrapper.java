package com.huixi.microspur.commons.util.wrapper;


import com.huixi.microspur.commons.errorcode.ErrorCodeEnum;
import com.huixi.microspur.commons.errorcode.IErrorCode;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;


/**
 * The class Wrapper.
 *
 * @param <T> the type parameter @author paascloud.net@gmail.com
 */
@Data
public class Wrapper<T> implements Serializable {

    /**
     * 序列化标识
     */
    private static final long serialVersionUID = 4893280118017319089L;

    /**
     * 编号.
     */
    @ApiModelProperty(value = "响应码",example = "200")
    private String code;

    /**
     * 信息.
     */
    @ApiModelProperty("响应消息")
    private String message;

    /**
     * 结果数据
     */
    @ApiModelProperty("响应结果")
    private T result;

    protected Wrapper(String code, String message, T data) {
        this.code = code;
        this.message = message;
        this.result = data;
    }

    /**
     * 成功返回结果
     *
     */
    public static <T> Wrapper<T> ok() {
        return new Wrapper<T>(ErrorCodeEnum.OK.getCode(), ErrorCodeEnum.OK.getMessage(), null);
    }

    /**
     * 成功返回结果
     *
     * @param data 获取的数据
     */
    public static <T> Wrapper<T> ok(T data) {
        return new Wrapper<T>(ErrorCodeEnum.OK.getCode(), ErrorCodeEnum.OK.getMessage(), data);
    }

    /**
     * 成功返回结果
     *
     * @param data 获取的数据
     * @param  message 提示信息
     */
    public static <T> Wrapper<T> ok(T data, String message) {
        return new Wrapper<T>(ErrorCodeEnum.OK.getCode(), message, data);
    }

    /**
     * 失败返回结果
     * @param errorCode 错误码
     */
    public static <T> Wrapper<T> error(IErrorCode errorCode) {
        return new Wrapper<T>(errorCode.getCode(), errorCode.getMessage(), null);
    }

    /**
     * 失败返回结果
     * @param errorCode 错误码
     * @param message 错误信息
     */
    public static <T> Wrapper<T> error(IErrorCode errorCode,String message) {
        return new Wrapper<T>(errorCode.getCode(), message, null);
    }

    /**
     * 失败返回结果
     * @param message 提示信息
     */
    public static <T> Wrapper<T> error(String message) {
        return new Wrapper<T>(ErrorCodeEnum.ERROR.getCode(), message, null);
    }

    /**
     * 失败返回结果
     */
    public static <T> Wrapper<T> error() {
        return error(ErrorCodeEnum.ERROR);
    }


    /**
     * 参数验证失败返回结果
     */
    public static <T> Wrapper<T> validateFailed() {
        return error(ErrorCodeEnum.VALIDATE_FAILED);
    }


    /**
     * 参数验证失败返回结果
     * @param message 提示信息
     */
    public static <T> Wrapper<T> validateFailed(String message) {
        return new Wrapper<T>(ErrorCodeEnum.VALIDATE_FAILED.getCode(), message, null);
    }

    /**
     * 登录异常返回结果
     */
    public static <T> Wrapper<T> unauthorized(T data) {
        return new Wrapper<T>(ErrorCodeEnum.UNAUTHORIZED.getCode(), ErrorCodeEnum.UNAUTHORIZED.getMessage(), data);
    }

    /**
     * 未授权返回结果
     */
    public static <T> Wrapper<T> forbidden(T data) {
        return new Wrapper<T>(ErrorCodeEnum.FORBIDDEN.getCode(), ErrorCodeEnum.FORBIDDEN.getMessage(), data);
    }

}

package com.huixi.microspur.commons.enums;

public enum EnableFlagEnum {

    ENABlED("Y"),
    FORBIDDEN("N");

    private String value;

    EnableFlagEnum(String value) {
        this.value = value;
    }

    public String value() {
        return this.value;
    }

}
